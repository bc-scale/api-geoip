from aiogeoip import requests_geoip as geoip, Geolocation


class ControllerGeoip:

    @staticmethod
    def get_geoip(ipv4: str) -> Geolocation:
        return geoip(ipv4)
