fastapi==0.70.0
uvicorn==0.15.0
aiogeoip
loguru==0.5.3
python-dotenv==0.19.2
starlette-exporter==0.11.0