import datetime
import time
import re

from typing import Optional

from fastapi import FastAPI, Request
from starlette_exporter import PrometheusMiddleware, handle_metrics

from .controller.controller_geoip import ControllerGeoip
from .version import __version__
from .config import logger

app = FastAPI()
app.add_middleware(PrometheusMiddleware, app_name='geoip', group_paths=True)
app.add_route("/metrics", handle_metrics)
up_in = datetime.datetime.now()


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    ipv4 = str(request.url).split('/')[-1]
    if re.findall(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', ipv4):
        logger.info(
            "GET geolocation from '%s' for ip '%s'" % (
                request.client.host, str(request.url).split('/')[-1]
            ))
    return response


@app.get("/uptime")
async def get_index():
    uptime = datetime.datetime.now() - up_in
    return {"uptime": "%s" % uptime}


@app.get("/ready")
async def get_ready():
    return {"status": "ok"}


@app.get("/v1/")
async def get_version():
    return {"version": __version__}


@app.get("/v1/{ipv4}")
async def get_geoip(ipv4: str, backend: Optional[str] = None, retry: Optional[int] = 3):
    """
    Obtém geolocalização a partir de endereço ipv4:

    - **ipv4**: IPv4 do endereço que deseja obter a geolocalização.
    - **backend**: (disabled) o backend que deseja utilizar para fazer a consulta (urllib3, requests, aiohttp).
    - **retry**: (disabled) número de tentativas caso a primeira falhar.

    Return:
    ```python
    {
        "query": str,
        "continent": str,
        "continentCode": str,
        "country": str,
        "countryCode": str,
        "region": str,
        "regionName": str,
        "city": str,
        "district": str,
        "zip": str,
        "lat": float,
        "lon": float,
        "timezone": str,
        "isp": str,
        "org": str,
        "as": str,
        "reverse": str
    }
    ```
    \f

    :param ipv4: endereço ipv4.
    :param backend: backend para fazer a consulta (urllib3, requests, aiohttp).
    :param ipretryv4: número de tentativas caso a primeira falhar.
    """
    return ControllerGeoip.get_geoip(ipv4)
